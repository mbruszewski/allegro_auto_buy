class ChangeLastRuntimeColumnType < ActiveRecord::Migration[5.2]
  def change
    remove_column :background_jobs, :last_runtime
    add_column :background_jobs, :last_runtime_sec, :float
  end
end
