class AddRuntimeToBackgroundJob < ActiveRecord::Migration[5.2]
  def change
    add_column :background_jobs, :last_runtime, :datetime
  end
end
