class CreateBackgroundJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :background_jobs do |t|
      t.string :status
      t.string :job_name
      t.integer :interval
      t.integer :account_id
      t.string :username_for_job
      t.string :search_products_by

      t.timestamps
    end
  end
end
