class BackgroundJobLog < ActiveRecord::Migration[5.2]
  def change
    add_column :background_jobs, :log, :text
  end
end
