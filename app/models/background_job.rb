require 'open-uri'

class BackgroundJob < ApplicationRecord
  belongs_to :account
  accepts_nested_attributes_for :account

  validates :account_id, presence: true

  validates :job_name, presence: true
  validates :interval, presence: true
  validates :username_for_job, presence: true
  validates :search_products_by, presence: true

  before_validation :provide_account_id
  after_create :start_worker

  state_machine :status,  initial: :new do
    event :error_found do
      transition [:new, :working] => :error
    end

    event :start_working do
      transition :new => :working
    end

    event :stop_working do
      transition [:new, :working] => :finished
    end
  end

  def add_log txt
    log_to_add = "[#{Time.zone.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] #{txt}"

    update_attributes(log: log_to_add + "\n" + self.log.to_s)
  end

  def start_worker
    self.job_name.constantize.perform_async({ background_job_id: self.id })
    #self.job_name.constantize.new.perform({ "background_job_id" => self.id })
  end

  def last_update_in_sec
    ((DateTime.now - DateTime.parse(self.updated_at.to_s))* 24 * 60 * 60).to_f
  end

  private
    def provide_account_id
      self.account_id = self.account.try(:id)
    end
end
