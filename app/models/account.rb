class Account < ApplicationRecord
  attr_accessor :password_first, :password_second

  has_many :background_jobs

  validates :username, presence: true
  #validates :password_first, presence: true, if: :password_blank?
  #validates :password_second, presence: true, if: :password_blank?
  validate :are_passwords_same

  before_save :encrypt_the_password

  private
  def encrypt_the_password
    hash = Digest::SHA256.new.digest(self.password_first)
    self.password = Base64.encode64(hash)
  end

  def are_passwords_same
    if password_first != password_second
      errors.add(:password_first, "Password first need to be same as second")
      errors.add(:password_second, "Password second need to be same as first")
    end
  end
end
