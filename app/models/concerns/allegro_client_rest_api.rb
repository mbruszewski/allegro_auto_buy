require 'rest-client'

class AllegroClientRestApi
  include ActiveModel::Model

  attr_accessor :username, :password, :webapi_key, :local_version, :country_code, :end_point, :redirect_path
  attr_reader :client, :session_handle

  def initialize(username, password)
    if Rails.env == "development"
      self.webapi_key = "a3ad47048f9546c9b2d00a919cc575e8"
      self.end_point = "https://api.allegro.pl.allegrosandbox.pl"
      self.redirect_path = "http://www.wp.pl"
    elsif Rails.env == "production"
      self.webapi_key = "ae33d8351eb74a9599bd958b1d14e81f"
      self.end_point = "https://api.allegro.pl"
      self.redirect_path = "http://www.wp.pl"
    end
  end

  def call_get(operation_name, locals = {})
    RestClient.get [self.end_point, operation_name].join("/"), locals
  end

  def call_post(operation_name, locals = {})
    RestClient.post [self.end_point, operation_name].join("/"), locals
  end

  def call(opertation_name, method, locals = {})
    RestClient::Request.execute(method: method, url: [self.end_point, operation_name].join("/"))
  end

  def authorize
    RestClient.get "https://allegro.pl/auth/oauth/authorize?response_type=code&client_id=#{self.webapi_key}&redirect_uri=#{self.redirect_path}"
  end

end
