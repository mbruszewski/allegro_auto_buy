require 'savon'

class AllegroClientWebApi
  include ActiveModel::Model

  attr_accessor :username, :password, :webapi_key, :local_version, :country_code, :end_point, :session_handle, :session_handle_time
  attr_reader :client

  START_ON_PRODUCTION = true

  def initialize(username, password)
    self.username = username
    self.password = password
    self.country_code = 1

    if Rails.env == "development" && !START_ON_PRODUCTION
      self.webapi_key = "a3ad47048f9546c9b2d00a919cc575e8"
      self.end_point = "https://webapi.allegro.pl.allegrosandbox.pl/service.php?wsdl"
    else
      self.webapi_key = "ae33d8351eb74a9599bd958b1d14e81f"
      self.end_point = "https://webapi.allegro.pl/service.php?wsdl"
    end
  end

  def call(operation_name, locals= {})
    client.call(operation_name, locals)
  end

  def login
    start_client
    message =  { user_login: username, user_hash_password: password, country_code: country_code, webapi_key: webapi_key, local_version: local_version }
    response = client.call(:do_login_enc, message: message)
    set_session_handle(response)
    self
  end

  def set_session_handle(login_response)
    @session_handle = login_response.body[:do_login_enc_response][:session_handle_part]
    @session_handle_time = DateTime.now
  end

  def should_get_new_session_handle?
    DateTime.now > @session_handle_time + 45.minutes
  end

  def get_user_item_list_not_parsed_params(search_username)
    message =  { webapi_key: webapi_key,
                 country_id: country_code,
                 filter_options: { item: { filter_id: 'userId', filter_value_id: { item: search_username } }},
                 sort_options: { sort_type: "startingTime", sort_order: "desc" },
                 result_size: 10
    }

    client.call(:do_get_items_list, message: message)
  end

  def get_user_item_list(search_username)
    response = get_user_item_list_not_parsed_params(search_username)

    items_count = response.body[:do_get_items_list_response][:items_count].to_i
    items = items_count > 0 ? response.body[:do_get_items_list_response][:items_list][:item] : []
    return items.class == Hash ? [items] : items
  end

  def get_user_item_list_with_title(search_username, title_names)
    response = get_user_item_list_not_parsed_params(search_username)

    items_count = response.body[:do_get_items_list_response][:items_count].to_i
    items = []
    if items_count > 0
      not_parsed_items = response.body[:do_get_items_list_response][:items_list][:item]
      not_parsed_items = not_parsed_items.class == "Hash" ? [not_parsed_items] : not_parsed_items

      items = not_parsed_items.map do |item|
        if title_names.split(";").any? { |title_name| item[:item_title].downcase.include?(title_name.downcase) } && item[:left_count].to_i >= 1
          price_info = item[:price_info][:item]

          helper = price_info.select { |item_price| item_price[:price_type] == "bidding" }.first
          bidding_price = helper.present? ? helper[:price_value].to_f : 0.0
          bidding_price_present = helper.present?

          helper = price_info.select { |item_price| item_price[:price_type] == "buyNow" }.first
          buynow_price = helper.present? ? helper[:price_value].to_f : 0.0
          buynow_price_present = helper.present?

          helper = price_info.select { |item_price| item_price[:price_type] == "withDelivery" }.first
          withdelivery_price = helper.present? ? helper[:price_value].to_f : 0.0
          withdelivery_price_present = helper.present?

          total_price = 0.0

          if buynow_price_present && bidding_price_present
            total_price = withdelivery_price - bidding_price + buynow_price
          else
            total_price = withdelivery_price
          end

          {
              id: item[:item_id],
              name: item[:item_title],
              price: total_price,
              buy_now: buynow_price > 0.0 ? true : false
          }
        end
      end
    end

    return items.compact
  end

  def get_user_id(username)
    message =  { country_id: country_code,
                 user_login: username,
                 webapi_key: webapi_key }

    response = client.call(:do_get_user_id, message: message)
    response.body[:do_get_user_id_response][:user_id]
  end

  def buy_item_now(item_id, price, quantity = 1)
    retries ||= 0

    message = {
        session_handle: @session_handle,
        bid_it_id: item_id,
        bid_user_price: price,
        bid_quantity: quantity,
        bid_buy_now: 1
    }

    Rails.logger.info client.call(:do_bid_item, message: message)
  rescue StandardError => e
    retry if (retries += 1) < 2
  end

  private
    def start_client
      if Rails.env == "development" && !START_ON_PRODUCTION
        @client = Savon.client do
          ssl_verify_mode :none
          wsdl "https://webapi.allegro.pl.allegrosandbox.pl/service.php?wsdl"
          log  false
          log_level  :debug
          pretty_print_xml true
          strip_namespaces true
        end
      else
        @client = Savon.client do
          ssl_verify_mode :none
          wsdl "https://webapi.allegro.pl/service.php?wsdl"
          log  false
          log_level  :debug
          pretty_print_xml true
          strip_namespaces true
        end
      end

      set_local_version
    end

    def set_local_version
      response = client.call(:do_query_all_sys_status, message: { country_id: country_code, webapi_key: webapi_key })
      self.local_version = response.body[:do_query_all_sys_status_response][:sys_country_status][:item][:ver_key]
    end
end
