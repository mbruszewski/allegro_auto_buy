require 'rest-client'
require 'nokogiri'
require 'open-uri'

class AllegroClientCrawl
  include ActiveModel::Model

  def user_item_list(search_username)
    retries ||= 0

    response = Nokogiri::HTML(open("https://allegro.pl/uzytkownik/#{search_username}?order=n"))

    newest_items = response.search("#opbox-listing section").last.search("article").map do |item|
      item_id = item["data-analytics-view-value"]
      item_name = item.search("a").first(2).last.children.text

      item_zl = item.children.last.children.last.children.last.children[1].children[0].children[0].children[0].children[0].text
      item_gr = item.children.last.children.last.children.last.children[1].children[0].children[0].children[0].children[2].text
      item_price = "#{item_zl}.#{item_gr}".to_f

      if_buy_now = item.children.last.children.last.children.last.children[1].at("span:contains('kup teraz')").try(:text).try(:strip).present?

      { id: item_id, name: item_name, price: item_price, if_buy_now: if_buy_now }
    end

    return newest_items
  rescue StandardError => e
    retry if (retries += 1) < 3
  end

  def item_list_filtered_by_word_and_buy_now(search_username, words)
    item_list = user_item_list(search_username)

    item_list.present? ? item_list.map { |item| item if item[:if_buy_now] == true && words.split(";").any? { |word| item[:name].downcase.include?(word.downcase) } }.compact : []
  end
end
