class AutoBuysController < ApplicationController
  def index
    @background_jobs = BackgroundJob.all
  end

  def new
    @background_job = BackgroundJob.new(job_name: "AutoBuyWorker")
    @background_job.build_account
  end

  def create
    @background_job = BackgroundJob.new(background_job_params)

    if @background_job.account.save && @background_job.save
      redirect_to auto_buys_path, notice: "Udało Ci się utworzyć zadanie automatycznego kupowania!"
    else
      render 'new'
    end
  end

  def edit
    @background_job = BackgroundJob.find(params[:id])
  end

  def update
    @background_job = BackgroundJob.find(params[:id])

    if @background_job.update_attributes(background_job_params)
      redirect_to auto_buys_path, notice: "Udało Ci się edytować zadanie automatycznego kupowania!"
    else
      render 'edit'
    end
  end

  def destroy
    @background_job = BackgroundJob.find(params[:id])

    if @background_job.status.in?(["error", "finished"]) && @background_job.delete
      redirect_to auto_buys_path, notice: "Udało Ci się usunąć zadania automatycznego kupowania"
    else
      redirect_to auto_buys_path, notice: "Nie udało Ci się usunąć zadania automatycznego kupowania"
    end
  end

  def stop_working
    @background_job = BackgroundJob.find(params[:id])

    if @background_job.stop_working
      redirect_to auto_buys_path, notice: "Udało Ci się zatrzymać zadania automatycznego kupowania"
    else
      redirect_to auto_buys_path, notice: "Nie udało Ci się zatrzymać zadania automatycznego kupowania"
    end
  end

  def retry_job
    @background_job = BackgroundJob.find(params[:id])

    @background_job.update_attributes(status: "new", log: nil)
    @background_job.start_worker

    redirect_to auto_buys_path, notice: "Zadanie przekazane do ponownego uruchomienia."
  end

  def show
    @background_job = BackgroundJob.find(params[:id])
  end

  private
    def background_job_params
      params.require(:background_job).permit(:job_name, :username_for_job, :interval, :search_products_by, account_attributes: [:username, :password_first, :password_second])
    end
end
