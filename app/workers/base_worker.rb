class BaseWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false

  def self.is_running?
    count_jobs_running > 0
  end

  def self.count_jobs_running
    #Sidekiq::Queue.new("default").map { |job| true if job.klass == self.class.to_s }.compact.count
    Sidekiq::Workers.new.map { |_pid, _tid, work| work }.count
  end

  def self.count_jobs_running_with_params(params)
    Sidekiq::Workers.new.map { |_pid, _tid, work| work }.map { |worker| worker if worker["payload"]["args"].first == params }.compact.count
  end

end