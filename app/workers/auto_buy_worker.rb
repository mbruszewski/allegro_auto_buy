class AutoBuyWorker < BaseWorker

  def perform options
    retries ||= 0

    iterate_times = 30
    background_job_id = options["background_job_id"]

    background_job = BackgroundJob.find(background_job_id)
    background_job.start_working
    background_job.reload

    iterate = 0

    account = background_job.account

    # login
    ac = AllegroClientWebApi.new(account.username, account.password)
    ac.login
    background_job.add_log "User logged in"

    user_id = ac.get_user_id(background_job.username_for_job)
    background_job.add_log "Took #{background_job.username_for_job} user id from allegro WebApi"

    items = ac.get_user_item_list_with_title(user_id, background_job.search_products_by)

    #crawl = AllegroClientCrawl.new
    #items = crawl.item_list_filtered_by_word_and_buy_now(background_job.username_for_job, background_job.search_products_by)

    background_job.add_log "Starting the buy loop"
    while background_job.status != "finished"
      date_start = DateTime.now if iterate == 0
      # go to items page
      #items_2 = crawl.item_list_filtered_by_word_and_buy_now(background_job.username_for_job, background_job.search_products_by)
      items_2 = ac.get_user_item_list_with_title(user_id, background_job.search_products_by)

      items_to_buy = items_2 - items
      # buy item
      items_to_buy.each do |item|
        background_job.add_log "Trying to buy item '#{item[:name]}' auction id '#{item[:id]}'"
        ac.buy_item_now(item[:id], item[:price], 1)
      end

      # set new item list as old
      items = items_2

      # sleep x seconds
      sleep background_job.interval

      iterate += 1
      # check for status change
      if iterate >= iterate_times
        retries = 0
        iterate = 0
        background_job.update_attributes(last_runtime_sec: ((DateTime.now - date_start) * 24.0 * 60.0 * 60.0 / iterate_times.to_f).to_f)
        background_job.reload

        ac.login if ac.should_get_new_session_handle?

        #logger.info "Petla zakonczona 30x - #{ac.session_handle_time} #{ac.session_handle}"
      end
    end
  rescue StandardError => e
    background_job.add_log e.message

    if (retries += 1) < 2
      retry
    else
      background_job.error_found
    end

    background_job.reload
  end

end