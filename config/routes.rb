require 'sidekiq/web'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :auto_buys do
    collection do
      get :stop_working
      get :retry_job
    end
  end

  mount Sidekiq::Web => '/sidekiq'
  root 'auto_buys#index'
end
